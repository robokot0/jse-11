package com.nlmkit.korshunov_am.tm.controller;

import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.enumerated.Role;
import com.nlmkit.korshunov_am.tm.service.UserService;

import java.util.List;

public class UserController extends AbstractController {
    /**
     * Сервис пользователей
     */
    private final UserService userService;
    /**
     * Конструктор
     * @param userService Сервис пользователей
     */
    public UserController(UserService userService) {
        this.userService = userService;
    }
    /**
     * Вычислить хэш строки
     * @param stringtohash строка для хэширования
     * @return хэш строки
     */
    public String getStringHash(String stringtohash) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(stringtohash.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }
    /**
     * Изменить данные пользователя
     * @param user пользовательё
     * @return 0 выполнено0
     */
    public int updateUserData(final User user){
        System.out.println("Please enter user login: ");
        final String login = scanner.nextLine();
        System.out.println("Please enter user role: ");
        final Role role = Role.valueOf(scanner.nextLine());
        System.out.println("Please enter user first name: ");
        final String firstName = scanner.nextLine();
        System.out.println("Please enter user second name: ");
        final String secondName = scanner.nextLine();
        System.out.println("Please enter user middle name: ");
        final String middleName = scanner.nextLine();
        userService.updateData(user.getId(),login,role,firstName,secondName,middleName);
        System.out.println("[OK]");
        return 0;
    }
    /**
     * Изменить пароль пользователя
     * @param user пользователь
     * @return 0 выполнено0
     */
    public int updateUserPassword(final User user){
        System.out.println("Please enter user password: ");
        final String password = scanner.nextLine();
        System.out.println("Please enter user password confirmation: ");
        final String passwordConfirmation = scanner.nextLine();
        if(!password.equals(passwordConfirmation)){
            System.out.println("[FAIL]");
            return 0;
        }
        userService.updatePassword(user.getId(),getStringHash(password));
        System.out.println("[OK]");
        return 0;
    }
    /**
     * Изменить данные пользователя по login
     * @return 0 выполнено
     */
    public int updateUserDataByLogin(){
        System.out.println("[UPDATE USER DATA BY LOGIN]");
        System.out.println("Please enter user login: ");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) System.out.println("[FAIL]");
        else updateUserData(user);
        return 0;
    }
    /**
     * Изменить пароль пользователя по login
     * @return 0 выполнено
     */
    public int updateUserPasswordByLogin(){
        System.out.println("[UPDATE USER PASSWORD BY LOGIN]");
        System.out.println("Please enter user login: ");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) System.out.println("[FAIL]");
        else updateUserPassword(user);
        return 0;
    }
    /**
     * Изменить данные пользователя по id
     * @return 0 выполнено
     */
    public int updateUserDataById(){
        System.out.println("[UPDATE USER DATA BY ID]");
        System.out.println("Please enter user id: ");
        final long id = scanner.nextLong();
        final User user = userService.findById(id);
        if (user == null) System.out.println("[FAIL]");
        else updateUserData(user);
        return 0;
    }
    /**
     * Изменить пароль пользователя по id
     * @return 0 выполнено
     */
    public int updateUserPasswordById(){
        System.out.println("[UPDATE USER PASSWORD BY ID]");
        System.out.println("Please enter user id: ");
        final long id = scanner.nextLong();
        final User user = userService.findById(id);
        if (user == null) System.out.println("[FAIL]");
        else updateUserPassword(user);
        return 0;
    }
    /**
     * Изменить данные пользователя по index
     * @return 0 выполнено
     */
    public int updateUserDataByIndex(){
        System.out.println("[UPDATE USER DATA BY INDEX]");
        System.out.println("Please enter user index: ");
        final int index = scanner.nextInt()-1;
        final User user = userService.findByIndex(index);
        if (user == null) System.out.println("[FAIL]");
        else updateUserData(user);
        return 0;
    }
    /**
     * Изменить пароль пользователя по index
     * @return 0 выполнено
     */
    public int updateUserPasswordByIndex(){
        System.out.println("[UPDATE USER PASSWORD BY INDEX]");
        System.out.println("Please enter user index: ");
        final int index = scanner.nextInt()-1;
        final User user = userService.findByIndex(index);
        if (user == null) System.out.println("[FAIL]");
        else updateUserPassword(user);
        return 0;
    }
    /**
     * Удалить пользователя по login
     * @return 0 выполнено
     */
    public int removeUserByLogin(){
        System.out.println("[REMOVE USER BY LOGIN]");
        System.out.println("Please enter user login: ");
        final String login = scanner.nextLine();
        final User userfinded = userService.findByLogin(login);
        if (userfinded == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        final User userdeleted = userService.removeById(userfinded.getId());
        if (userdeleted == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }
    /**
     * Удалить пользователя по id
     * @return 0 выполнено
     */
    public int removeUserById(){
        System.out.println("[REMOVE USER BY ID]");
        System.out.println("Please enter user id: ");
        final long id = scanner.nextLong();
        final User userdeleted = userService.removeById(id);
        if (userdeleted == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }
    /**
     * Удалить пользователя по index
     * @return 0 выполнено
     */
    public int removeUserByIndex(){
        System.out.println("[REMOVE USER BY INDEX]");
        System.out.println("Please enter user index: ");
        final int index = scanner.nextInt()-1;
        final User userfinded = userService.findByIndex(index);
        if (userfinded == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        final User userdeleted = userService.removeById(userfinded.getId());
        if (userdeleted == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }
    /**
     * Создать пользователя
     * @return 0 выполнено
     */
    public int createUser(){
        System.out.println("[CREATE USER]");
        System.out.println("Please enter user login: ");
        final String login = scanner.nextLine();
        System.out.println("Please enter user role: ");
        final Role role = Role.valueOf(scanner.nextLine());
        System.out.println("Please enter user first name: ");
        final String firstName = scanner.nextLine();
        System.out.println("Please enter user second name: ");
        final String secondName = scanner.nextLine();
        System.out.println("Please enter user middle name: ");
        final String middleName = scanner.nextLine();
        System.out.println("Please enter user password: ");
        final String password = scanner.nextLine();
        System.out.println("Please enter user password confirmation: ");
        final String passwordConfirmation = scanner.nextLine();
        if(!password.equals(passwordConfirmation)){
            System.out.println("[FAIL]");
            return 0;
        }
        userService.create(login,role,firstName,secondName,middleName,getStringHash(password));
        System.out.println("[OK]");
        return 0;
    }
    /**
     * Показать информацию по пользователю
     * @param user пользователь
     */
    public void viewUser(final User user) {
        if (user == null) return;
        System.out.println("[VIEW USER]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("ROLE: " + user.getRole().toString());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("SECOND NAME: " + user.getSecondName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("PASSWORD HASH: " + user.getPasswordHash());
        System.out.println("[OK]");
    }
    /**
     * Показать пользователя по login
     * @return 0 выполнено
     */
    public int viewUserByLogin() {
        System.out.println("Enter, user login:");
        final String login = scanner.nextLine();
        final  User user = userService.findByLogin(login);
        viewUser(user);
        return 0;
    }
    /**
     * Показать пользователя по id
     * @return 0 выполнено
     */
    public int viewUserById() {
        System.out.println("Enter, user id:");
        final long id = scanner.nextLong();
        final  User user = userService.findById(id);
        viewUser(user);
        return 0;
    }
    /**
     * Показать пользователя по index
     * @return 0 выполнено
     */
    public int viewUserByIndex() {
        System.out.println("Enter, user index:");
        final int index = scanner.nextInt()-1;
        final User user = userService.findByIndex(index);
        viewUser(user);
        return 0;
    }
    /**
     * Показать список пользователей
     * @param users список
     */
    public void viewUsers(List<User> users) {
        if (users == null || users.isEmpty()) return;
        int index = 1;
        for (final User user: users) {
            System.out.println(index + ". " + user.getLogin()+ ": " + user.getRole()+ ": " + user.getFirstName()+ ": " + user.getSecondName()+ ": " + user.getMiddleName());
            index ++;
        }
    }
    /**
     * Показать список пользователей
     * @return 0 выполнено
     */
    public int listUser(){
        System.out.println("[LIST USER]");
        viewUsers(userService.findAll());
        System.out.println("[OK]");
        return 0;
    }
}

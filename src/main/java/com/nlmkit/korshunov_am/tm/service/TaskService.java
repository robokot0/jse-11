package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.repository.TaskRepostory;
import com.nlmkit.korshunov_am.tm.entity.Task;

import java.util.Collections;
import java.util.List;

/**
 * Сервис задач
 */
public class TaskService {
    /**
     * Репозитарий задач
     */
    private final TaskRepostory taskRepostory;

    /**
     * Конструтор
     * @param taskRepostory Репозитарий задач
     */
    public TaskService(final TaskRepostory taskRepostory) {
        this.taskRepostory = taskRepostory;
    }

    /**
     * Создать задачу
     * @param name имя
     * @return задача
     */
    public Task create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepostory.create(name);
    }

    /**
     * Создать задачу
     * @param name имя
     * @param description описание
     * @return задача
     */
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepostory.create(name, description);
    }

    /**
     * Измениь задачу
     * @param id идентификатор
     * @param name имя
     * @param description описание
     * @return задача
     */
    public Task update(final Long id, final String name, final String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepostory.update(id, name, description);
    }

    /**
     * Удалить все задачи
     */
    public void clear() {
        taskRepostory.clear();
    }

    /**
     * Найти по индексу
     * @param index индекс
     * @return задача
     */
    public Task findByIndex(final int index) {
        if (index < 0 || index >= taskRepostory.size()) return null;
        return taskRepostory.findByIndex(index);
    }

    /**
     * Найти по имени
     * @param name имя
     * @return задача
     */
    public Task findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepostory.findByName(name);
    }

    /**
     * Найти по идентификатору
     * @param id идентификатор
     * @return задча
     */
    public Task findById(final Long id) {
        if (id == null) return null;
        return taskRepostory.findById(id);
    }

    /**
     * Удалить по индексу
     * @param index индекс
     * @return задача
     */
    public Task removeByIndex(final int index) {
        if (index < 0 || index >= taskRepostory.size()) return null;
        return taskRepostory.removeByIndex(index);
    }

    /**
     * Удалить по идентификаотру
     * @param id идентификатор
     * @return задача
     */
    public Task removeById(final Long id) {
        if (id == null) return null;
        return taskRepostory.removeById(id);
    }

    /**
     * Удалить по имени
     * @param name имя
     * @return задача
     */
    public Task removeByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepostory.removeByName(name);
    }
    /**
     * Получить все задачи проекта
     * @param projectId
     * @return список задач
     */
    public List<Task> findAllByProjectId(Long projectId) {
        if (projectId == null) return null;
        return taskRepostory.findAllByProjectId(projectId);
    }
    /**
     * Получить все задачи
     * @return список задач
     */
    public List<Task> findAll() {
        return taskRepostory.findAll();
    }

    /**
     * Получить задачу по проекту и ид
     * @param projectId ид проекта
     * @param id ид
     * @return задача
     */
    public Task findByProjectIdAndId(Long projectId, Long id) {
        if (projectId == null) return null;
        if (id == null) return null;
        return taskRepostory.findByProjectIdAndId(projectId, id);
    }
}

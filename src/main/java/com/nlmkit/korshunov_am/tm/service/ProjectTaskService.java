package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.entity.Project;
import com.nlmkit.korshunov_am.tm.entity.Task;
import com.nlmkit.korshunov_am.tm.repository.ProjectRepository;
import com.nlmkit.korshunov_am.tm.repository.TaskRepostory;

import java.util.Collections;
import java.util.List;

/**
 * Сервис задачи в проекте
 */
public class ProjectTaskService {
    /**
     * Репозитарий проектов
     */
    private final ProjectRepository projectRepository;
    /**
     * Репозитарий задач
     */
    private final TaskRepostory taskRepostory;

    /**
     * Конструктор
     * @param projectRepository репозитарий проектов
     * @param taskRepostory репозитарий задач
     */
    public ProjectTaskService(ProjectRepository projectRepository, TaskRepostory taskRepostory) {
        this.projectRepository = projectRepository;
        this.taskRepostory = taskRepostory;
    }

    /**
     * Получить список задач проекта
     * @param projectId идентификатор проекта
     * @return список задач
     */
    public List<Task> findAllByProjectId(final Long projectId) {
        if (projectId == null) return Collections.emptyList();
        return taskRepostory.findAllByProjectId(projectId);
    }

    /**
     * Удалить задачу из проекта
     * @param projectId идентификатор проекта
     * @param taskId идентификатор задачи
     * @return задача
     */
    public Task removeTaskFromProject(final Long projectId, final Long taskId) {
        if (projectId == null) return null;
        if (taskId == null) return null;
        final Task task = taskRepostory.findByProjectIdAndId(projectId,taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    /**
     * Добавить задачу к проекту
     * @param projectId идентификатор проекта
     * @param taskId идентификатор задачи
     * @return задача
     */
    public Task addTaskToProject(final Long projectId, final Long taskId) {
        final Project project = projectRepository.findById(projectId);
        if (project == null) return null;
        final Task task = taskRepostory.findById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }


}
